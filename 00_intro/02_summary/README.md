# Summary

- This chapter provides a high-level overview of Cisco’s career certifications and how candidates can choose their own destiny by picking the areas where they want to build experience
and become certified.

- This chapter describes Cisco’s new specialist exams, which focus on many different technologies, such as Firepower, SD-WAN, and IoT. This chapter also discusses some of the benefits of becoming certified, from career advancement to building confidence to commanding a higher salary in the workplace.

- This chapter also details at a high level the components of Cisco DevNet, the value of the DevNet community, and DevNet events such as Cisco DevNet Express.

- Finally, this chapter introduces DevNet tools such as DevNet Automation Exchange and DevNet learning labs.