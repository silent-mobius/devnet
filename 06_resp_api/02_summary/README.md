# rest api fundamentals

## api types

## api access types

## http basics

## uniform resource locator

## method

## rest method and crud

## get and post

## http headers

## request headers

## response headers

## response codes

## xml

## json

## yaml

## web-hook

## tools for all this mess

## sequence diagram


---

# rest api constraints

## client/server

## stateless

## cace 

## uniform interface

## layered system

## code on demand

## rest api versioning

## pagination

## rate limit and monetization

## rate limit on client side

---

# rest tools

## postman

## curl 

## httpie

## Python requests